#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include "handler.h"
#include "recursive.h"


int columns_with(int pallet_l, int pallet_w, int case_l, int case_w, int actual_max) {
	int max = actual_max;
	int tmp = 0;
	int one_cases_rows, one_cases_cols, one_sub_pallet_l, one_sub_pallet_w;
	
	one_cases_cols 		= pallet_l / case_l;
	one_cases_rows 		= pallet_w / case_w;
	one_sub_pallet_l 	= one_cases_cols * case_l;
	one_sub_pallet_w 	= one_cases_rows * case_w;
	
	tmp = getNFrom(one_cases_rows, one_cases_cols, 0, 0, 0, 0, 0, 0);
	if (tmp > max)
		max = tmp;	
	
	return max;
}


int cross_two_with(int pallet_l, int pallet_w, int case_l, int case_w, int actual_max) {
	int max = actual_max;
	int tmp = 0;
	int one_cases_rows, one_cases_cols, one_sub_pallet_l, one_sub_pallet_w;
	int two_cases_rows, two_cases_cols, two_sub_pallet_l, two_sub_pallet_w;
	int one_rows, one_cols;
	
	one_sub_pallet_l = pallet_l - case_w;
	one_sub_pallet_w = pallet_w;
	
	one_cols 		 = one_sub_pallet_l / case_l;
	one_rows 	 	 = one_sub_pallet_w / case_w;
				
	// Decrement the vertical boxes and increment the horizontal
	for (int one_c = one_cols; one_c > 0; one_c--) {
		one_cases_rows 		= one_rows;
		one_cases_cols 		= one_c;

		// Number of vertical boxes
		one_sub_pallet_l 	= one_c * case_l;
		one_sub_pallet_w 	= one_rows  * case_w;
		
	    // Calculate remaining space
		two_sub_pallet_l 	= pallet_l - one_sub_pallet_l;
		two_sub_pallet_w 	= pallet_w;

		two_cases_cols 		= two_sub_pallet_l / case_w;
		two_cases_rows   	= two_sub_pallet_w / case_l;

		two_sub_pallet_l 	= two_cases_cols * case_w;
        two_sub_pallet_w 	= two_cases_rows * case_l;
		
		tmp = getNFrom(one_cases_rows, one_cases_cols, two_cases_rows, two_cases_cols, 0, 0, 0, 0);
		if (tmp > max)
			max = tmp;					
	}
	return max;
}


int cross_three_with(int pallet_l, int pallet_w, int case_l, int case_w, int actual_max) {
	int max = actual_max;
	int tmp = 0;
	int one_cases_rows, one_cases_cols, one_sub_pallet_l, one_sub_pallet_w;
	int two_cases_rows, two_cases_cols, two_sub_pallet_l, two_sub_pallet_w;
	int four_cases_rows, four_cases_cols, four_sub_pallet_l, four_sub_pallet_w;
	int one_rows, one_cols;
	int two_rows, two_cols;
	
	one_sub_pallet_l 	= pallet_l - case_w;
	one_sub_pallet_w 	= pallet_w;
	one_cols 			= one_sub_pallet_l / case_l;
	one_rows	 		= one_sub_pallet_w / case_w;
	
	for (int one_r = one_rows; one_r > 0; one_r--) {
		for (int one_c = one_cols; one_c > 0; one_c--) {
			one_cases_rows   = one_r;
			one_cases_cols   = one_c;

			one_sub_pallet_l = one_c * case_l;
			one_sub_pallet_w = one_r * case_w;

            one_sub_pallet_l = one_sub_pallet_l;
			one_sub_pallet_w = one_sub_pallet_w;

            // Calculate remaining space
			two_sub_pallet_l = pallet_l - one_sub_pallet_l;
			two_sub_pallet_w = pallet_w - case_l;

			two_rows 	 	 = two_sub_pallet_w / case_l;
			two_cols 	 	 = two_sub_pallet_l / case_w;
			
			for (int two_r = two_rows; two_r > 0; two_r--) {
				for (int two_c = two_cols; two_c > 0; two_c--) {
					two_cases_cols     = two_c;
					two_cases_rows     = two_r;

					two_sub_pallet_l   = two_c * case_w;
					two_sub_pallet_w   = two_r * case_l;

					two_sub_pallet_l   = two_sub_pallet_l;
					two_sub_pallet_w   = two_sub_pallet_w;

					four_sub_pallet_l  = pallet_l;
					four_sub_pallet_w  = pallet_w - std::max(one_sub_pallet_w, two_sub_pallet_w);

					four_cases_cols    = four_sub_pallet_l / case_w;
					four_cases_rows    = four_sub_pallet_w / case_l;
                                           					
					four_sub_pallet_l  = four_cases_cols * case_w;
					four_sub_pallet_w  = four_cases_rows * case_l;
					
					tmp = getNFrom(one_cases_rows, one_cases_cols, two_cases_rows, two_cases_cols, 0, 0, four_cases_rows, four_cases_cols);
					if (tmp > max)
						max = tmp;
				}                                                        
			}
		}
	} 
	return max;       
}


int cross_four_with(int pallet_l, int pallet_w, int case_l, int case_w, int actual_max) {
	int max = actual_max;
	int tmp = 0;
	int one_cases_rows, one_cases_cols, one_sub_pallet_l, one_sub_pallet_w;
	int two_cases_rows, two_cases_cols, two_sub_pallet_l, two_sub_pallet_w;
	int three_cases_rows, three_cases_cols, three_sub_pallet_l, three_sub_pallet_w;
	int four_cases_rows, four_cases_cols, four_sub_pallet_l, four_sub_pallet_w;
	int one_rows, one_cols;
	int two_rows, two_cols;
	int three_rows, three_cols;
	int four_rows, four_cols;
	
	one_sub_pallet_l  = pallet_l - case_w;
	one_sub_pallet_w  = pallet_w - case_l;

	one_rows          = one_sub_pallet_w / case_w;
	one_cols          = one_sub_pallet_l / case_l;
	
	for (int one_r = one_rows; one_r > 0; one_r--) {
		for (int one_c = one_cols; one_c > 0; one_c--) {
			one_cases_rows   = one_r;
			one_cases_cols   = one_c;

			one_sub_pallet_l = one_c * case_l;
			one_sub_pallet_w = one_r * case_w;

			one_sub_pallet_l = one_sub_pallet_l;
			one_sub_pallet_w = one_sub_pallet_w;

			two_sub_pallet_l = pallet_l - one_sub_pallet_l;
			two_sub_pallet_w = pallet_w - case_w;

			two_rows         = two_sub_pallet_w / case_l;
			two_cols         = two_sub_pallet_l / case_w;
			
			for (int two_r = two_rows; two_r > 0; two_r--) {
				for (int two_c = two_cols; two_c > 0; two_c--) {
					two_cases_rows 	   = two_r;
					two_cases_cols     = two_c;

					two_sub_pallet_l   = two_c * case_w;
					two_sub_pallet_w   = two_r * case_l;

					two_sub_pallet_l   = two_sub_pallet_l;
					two_sub_pallet_w   = two_sub_pallet_w;

					three_sub_pallet_w = pallet_w - two_sub_pallet_w;
					three_rows         = three_sub_pallet_w / case_w;
					three_sub_pallet_l = (pallet_w - one_sub_pallet_w) >= (three_rows * case_w) ? (pallet_l - case_w) : (pallet_l - std::max(case_w, one_sub_pallet_l));
					three_cols         = three_sub_pallet_l / case_l;
					
					for (int three_r = three_rows; three_r > 0; three_r--) {
						for (int three_c = three_cols; three_c > 0; three_c--) {
							three_cases_rows   = three_r;
							three_cases_cols   = three_c;

							three_sub_pallet_l = three_c * case_l;
							three_sub_pallet_w = three_r * case_w;

							three_sub_pallet_l = three_sub_pallet_l;
							three_sub_pallet_w = three_sub_pallet_w;

							four_sub_pallet_l  = pallet_l - three_sub_pallet_l;
							four_cols          = four_sub_pallet_l / case_w;
							four_sub_pallet_w  = (pallet_l - two_sub_pallet_l) >= (four_cols * case_w) ? (pallet_w - one_sub_pallet_w) : (pallet_w - std::max(one_sub_pallet_w, two_sub_pallet_w));

							four_cases_cols    = four_cols;
							four_cases_rows    = four_sub_pallet_w / case_l;

							four_sub_pallet_l  = four_cases_cols * case_w;
							four_sub_pallet_w  = four_cases_rows * case_l;
							
							tmp = getNFrom(one_cases_rows, one_cases_cols, two_cases_rows, two_cases_cols, three_cases_rows, three_cases_cols, four_cases_rows, four_cases_cols);
							if (tmp > max)
								max = tmp;
						}
					}
				}
			}
		}
	}
	return max;
}

int genius_with(int pallet_l, int pallet_w, int case_l, int case_w, int actual_max) {
	return rec_genius_with(pallet_l, pallet_w, case_l, case_w, actual_max);
}

// Obtain n and cases vertices
std::string genius_with_vertices_for(int pallet_l, int pallet_w, int case_l, int case_w) {
	return rec_genius_with_vertices_for(pallet_l, pallet_w, case_l, case_w);
}

// Needed for SWIG -  DO NOT REMOVE
int main() {
	return 0;
}
