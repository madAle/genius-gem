#include <algorithm>
#include <stdio.h>
#include <stdlib.h>

int **vertices;

/******************************************************************
 ******************************************************************/

/**
 * Read the solution file and store the positions of the rectangles in
 * the "vertices" array.
 */
void readSolutionFile(int *n, int *L, int *W) {
  int i;

  FILE *solutionFile = fopen("solution.txt", "r");
  if(solutionFile == NULL) {
    printf("ERROR: It was not possible to open the file solution.txt.\n");
    exit(0);
  }

  fscanf(solutionFile, "%d %d %d", n, L, W);
  vertices = (int**) malloc((*n + 4)*sizeof(int*));

  if(vertices == NULL) {
    printf("ERROR: Error in memory allocation while reading solution file.\n");
    exit(0);
  }
  for(i = 0; i < *n; i++) {
    vertices[i] = (int*) malloc(4 * sizeof(int));
    if(vertices[i] == NULL) {
      printf("ERROR: Error in memory allocation while reading solution file.\n");
      exit(0);
    }
  }

  for(i = 0; i < *n; i++) {
    fscanf(solutionFile, "%d %d %d %d",
           &vertices[i][0], &vertices[i][1], &vertices[i][2], &vertices[i][3]);
  }

  fclose(solutionFile);
}

/******************************************************************
 ******************************************************************/

/**
 * Create the MetaPost file containing the graphical representation
 * of the solution.
 */
void writeMetaPostFile(int n, int L, int W) {
  int i;
  FILE *metaPostFile;

  double u = 20.0 / L;

  metaPostFile = fopen("solution.mp", "w");
  if(metaPostFile == NULL) {
    printf("ERROR: It was not possible to open the file solution.mp.\n");
    exit(0);
  }

  fprintf(metaPostFile,"verbatimtex\n"
          "%c&latex\n"
          "\\documentclass{article}\n"
          "\\begin{document}\n"
          "etex\n"
          "beginfig(-1);\n"
          "u=%fcm;\n", '%', u);

  fprintf(metaPostFile,"path p;"
          "p := (%du,%du)--(%du,%du)--(%du,%du)--(%du,%du)--cycle;\n"
          "fill p withcolor 0.2black + 0.7 white;\n"
          "deltaX = %du;\n"
          "stepX = 11.5;\n"
          "for x = -%du step stepX until %du:\n"
          "  draw (x, 0u)--(x + deltaX, %du) withcolor 0.1 white;\n"
          "endfor;\n"
          "clip currentpicture to p;\n"
          "draw p withcolor 0.2black + 0.7 white;\n",
          vertices[0][0], vertices[0][1], vertices[0][0], vertices[0][3],
          vertices[0][2], vertices[0][3], vertices[0][2], vertices[0][1],
          vertices[0][3], vertices[0][2], vertices[0][2], vertices[0][3]);

  /* Pallet. */
  fprintf(metaPostFile, "draw (%du,%du)--(%du,%du)--(%du,%du)--(%du,%du)"
          "--cycle;\n",
          vertices[0][0], vertices[0][1], vertices[0][0], vertices[0][3],
          vertices[0][2], vertices[0][3], vertices[0][2], vertices[0][1]);
  
  /* Rectangles. */
  for(i = 1; i < n; i++) {
    
    fprintf(metaPostFile, "fill (%du,%du)--(%du,%du)--(%du,%du)--(%du,%du)"
            "--cycle withcolor white;\n",
             vertices[i][0], vertices[i][1], vertices[i][0], vertices[i][3],
             vertices[i][2], vertices[i][3], vertices[i][2], vertices[i][1]);

    fprintf(metaPostFile, "draw (%du,%du)--(%du,%du)--(%du,%du)--(%du,%du)"
            "--cycle;\n",
             vertices[i][0], vertices[i][1], vertices[i][0], vertices[i][3],
             vertices[i][2], vertices[i][3], vertices[i][2], vertices[i][1]);

    /* Label. */
    /*
    fprintf(metaPostFile,"label(btex $%d$ etex scaled 1,(%fu,%fu));\n", i,
            vertices[i][0] + ((double)(vertices[i][2] - vertices[i][0]))/2.0,
            vertices[i][1] + ((double)(vertices[i][3] - vertices[i][1]))/2.0);
    */
  }

  fprintf(metaPostFile, "endfig;\nend;\n");
  fclose(metaPostFile);
}

/******************************************************************
 ******************************************************************/

/**
 * Create the MetaPost file containing the graphical representation
 * of the solution.
 */
void writeSVGFile(int n, int L, int W) {
  int i;
  FILE *SVGFile;

  double scale = 600.0 / (double) L;

  SVGFile = fopen("solution.svg", "w");
  if(SVGFile == NULL) {
    printf("ERROR: It was not possible to open the file solution.svg.\n");
    exit(0);
  }

  fprintf(SVGFile,"<?xml version=\"1.0\" standalone=\"no\"?>\n"
          "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" "
          "\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n"
          "<svg width=\"%f\" height=\"%f\" version = \"1.1\"\n"
          "xmlns=\"http://www.w3.org/2000/svg\">\n",
          L*scale, W*scale);

  fprintf(SVGFile, "<g transform=\"scale(%f)\">\n", scale);

  /* Pallet. */  
  fprintf(SVGFile,
          "<rect x = \"%d\" y = \"%d\" width = \"%d\" height = \"%d\" "
          "fill = \"gray\" stroke = \"black\" stroke-width = \"%f\"/>\n",
          0, 0, L, W, 1.0 / scale);

  /* Rectangles. */
  for(i = 1; i < n; i++) {

    fprintf(SVGFile,
            "<rect x = \"%d\" y = \"%d\" width = \"%d\" height = \"%d\" "
            "fill = \"white\" stroke = \"black\" stroke-width = \"%f\"/>\n",
            vertices[i][0], W - vertices[i][3],
            vertices[i][2] - vertices[i][0],
            vertices[i][3] - vertices[i][1], 1.0 / scale);

    /* Label. */
    /*
    fprintf(SVGFile,
            "<text x = \"%f\" y = \"%f\" fill = \"black\" "
            "font-size = \"%f\">%d</text>\n",
            vertices[i][0] + (vertices[i][2] - vertices[i][0])/2.0,
            W - vertices[i][3] + (vertices[i][3] - vertices[i][1])/2.0,
            std::min(vertices[i][2] - vertices[i][0],
                     vertices[i][3] - vertices[i][1])/4.0, i);
    */
  }

  fprintf(SVGFile, "</g>\n");
  fprintf(SVGFile,"</svg>");

  fclose(SVGFile);
}

/******************************************************************
 ******************************************************************/

void makeGraphics() {
  int i, n, L, W;

  readSolutionFile(&n, &L, &W);

  writeMetaPostFile(n, L, W);

  writeSVGFile(n, L, W);

  for(i = 0; i < n; i++) {
    free(vertices[i]);
  }
  free(vertices);
}
