# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'genius/version'

Gem::Specification.new do |spec|
  spec.name          = "genius"
  spec.version       = Genius::VERSION
  spec.authors       = ["Alessandro Verlato"]
  spec.email         = ["averlato@gmail.com"]
  spec.description   = %q{Freakin' genius people will use this gem}
  spec.summary       = %q{Freakin' genius people will use this gem}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib", "ext"]
  spec.extensions    = %w[ext/genius/extconf.rb]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
