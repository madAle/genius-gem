#include <string>

int columns_with(int pallet_l, int pallet_w, int case_l, int case_w, int actual_max);

int cross_two_with(int pallet_l, int pallet_w, int case_l, int case_w, int actual_max);

int cross_three_with(int pallet_l, int pallet_w, int case_l, int case_w, int actual_max);

int cross_four_with(int pallet_l, int pallet_w, int case_l, int case_w, int actual_max);

int genius_with(int pallet_l, int pallet_w, int case_l, int case_w, int actual_max);

// Obtain n and cases vertices
std::string genius_with_vertices_for(int pallet_l, int pallet_w, int case_l, int case_w);
