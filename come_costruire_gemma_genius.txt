Dopo aver apportato le modifiche necessarie ai file sorgenti:

- sudo ./compila   (file che contiene già i comandi necessari)
- test della gemma: 
  -> irb
  -> require 'recursive'
  -> <<< chiamare le funzioni >>>
 
- se funziona tutto bene, 
  -> aggiornare il file version.rb nella cartella genius/lib/genius/
  -> dalla cartella principale della gemma (genius) lanciare:
    => gem build genius.gemspec
    => se va tutto a buon fine dovremmo avere la nuova versione della gemma, ad esempio "genius-0.0.5.gem" nella cartella 		 		principale (genius)
	
- per installare localmente (dentro all'app rails Palmap) la gemma, bisogna spostarsi nella cartella dove si desidera che risieda la gemma (sempre all'interno dell'app rails!)
Attualmente genius è in lib/genius.
Copiare il .gem nella cartella lib/genius/gems  (la versione vecchia si può anche eliminare da li)
Dalla cartella lib/genius lanciare:    gem generate_index
	