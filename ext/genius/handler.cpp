#include <cmath>
#include <cstdio>
#include <cstdlib>

int getNFrom(int one_cases_rows, int one_cases_cols, int two_cases_rows, int two_cases_cols, int three_cases_rows, int three_cases_cols, int four_cases_rows, int four_cases_cols) {
	return ((one_cases_rows * one_cases_cols) + (two_cases_rows * two_cases_cols) + (three_cases_rows * three_cases_cols) + (four_cases_rows * four_cases_cols));
}