/* 
 * Author: averlato
 *
 * Created on 9 novembre 2012, 22.39
 */

#ifndef RECPARTALGORITHM_H
#define	RECPARTALGORITHM_H

#include <string>


int rec_genius_with(int pallet_l, int pallet_w, int case_l, int case_w, int actual_max);

// Obtain n and cases vertices
std::string rec_genius_with_vertices_for(int pallet_l, int pallet_w, int case_l, int case_w);
	

#endif	/* RECPARTALGORITHM_H */

