#ifndef       DRAW_H_
# define      DRAW_H_


/**
 * Determine the orientation of the boxes (l,w) that maximize the
 * homogeneous packing in (x,y).
 *
 * Parameters:
 * x - Length of the rectangle.
 *
 * y - Width of the rectangle.
 */
short boxOrientation(int x, int y);

int drawBD(int L, int W, int ret);


#endif
